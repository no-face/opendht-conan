from conans import ConanFile, CMake
import os

username = os.getenv("CONAN_USERNAME", "noface")
channel = os.getenv("CONAN_CHANNEL", "testing")
version = "1.3.0"
package = "opendht"

class TestRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "%s/%s@%s/%s" % (package, version, username, channel)
    generators = "cmake"

    build_requires = "cmake_installer/1.0@conan/stable"

    default_options = (
        "cmake_installer:version=3.6.3"
    )

    def build(self):
	cmake = CMake(self)

        cmake.configure(source_dir = self.conanfile_directory, build_dir = ".")
        cmake.build()

    def imports(self):
        self.copy("*.dll", "bin", "bin")
        self.copy("*.dylib", "bin", "bin")

    def test(self):
        self.run(os.path.join("bin","example"))

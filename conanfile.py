from conans import ConanFile, CMake, tools
import os
from os import path

class OpendhtConan(ConanFile):
    name        = "opendht"
    version     = "1.3.0"
    license     = "GPLv3"
    description = "A lightweight C++11 Distributed Hash Table implementation"
    url         = "https://github.com/paulobrizolara/opendht-conan"
    repo_url    = "https://github.com/savoirfairelinux/opendht/"
    settings    = "os", "compiler", "arch"
    generators  = "cmake"
    exports     = ("CMakeLists.txt",)

    options     = {
        "shared": [True, False], 
        "build_tools" : [True, False],
        "use_pic" : [True, False]
    }
    default_options = (
        "shared=False",
        "build_tools=False",
        "use_pic=True",

        "msgpack-c:header_only=False",
        "cmake_installer:version=3.6.3"
    )

    requires = (
        "gnutls/3.5.11@noface/testing",
        "gmp/6.1.2@noface/testing",
        "nettle/3.3@noface/testing",
        "msgpack-c/1.4.2@noface/stable"
    )

    build_requires = "cmake_installer/1.0@conan/stable"

    ZIP_URL = "%s/archive/%s.tar.gz" % (repo_url, version)
    FILE_SHA = '9a479ac3ffce481a942a7be238fe5ec3b1a5c0b9be7bdd7f11b5d0b39dec1abf'
    
    #Folder inside the zip
    INSIDE_DIR = "opendht-" + version
    UNZIPPED_DIR = "opendht"

    def source(self):
        zip_name = "%s.tar.gz" % self.name

        tools.download(self.ZIP_URL, zip_name)
        tools.check_sha256(zip_name, self.FILE_SHA)
        tools.untargz(zip_name)
        #os.unlink(zip_name)
        
        os.rename(self.INSIDE_DIR, self.UNZIPPED_DIR)

    def build(self):
        self.apply_patches()

        build_dir = os.path.join(self.build_folder, "build")

        cmake = CMake(self)
        
        self.cmake_configure(cmake, build_dir)
        self.cmake_build_and_install(cmake, build_dir)
        
    def package(self):
        # copy only include files, because lib files should be installed by cmake
        self.copy("**.h", src=path.join(self.UNZIPPED_DIR, "include"), dst="include", keep_path=True)

    def package_info(self):
        self.cpp_info.libs      = ["opendht"]


####################################### Helpers ################################################

    def apply_patches(self):
        if self.settings.os != "Android":
            return

        src_dir = os.path.join(self.UNZIPPED_DIR, "src")

        # Some android versions, has no (full) support to std::to_string
        previous = 'DHT_LOG.d(hash, node->id, "[store %s] [node %s] refreshed value %s", ' \
                    + 'hash.toString().c_str(), node->toString().c_str(), std::to_string(vid).c_str())'
        replaced = 'DHT_LOG.d(hash, node->id, "[store %s] [node %llu] refreshed value %s", ' \
                    + 'hash.toString().c_str(), node->toString().c_str(), vid)'
        tools.replace_in_file(path.join(src_dir, "dht.cpp"), previous, replaced)

        # Fix wrong "std"
        tools.replace_in_file(path.join(src_dir, "callbacks.cpp"), "std::exp2", "exp2")

    def cmake_configure(self, cmake, build_folder):
        cmake.configure(
            defs        = self.cmake_defs(),
            source_dir  = self.conanfile_directory,
            build_dir   = build_folder)
        
    def cmake_build_and_install(self, cmake, build_dir):
        cmd = 'cmake --build . --target install %s' % (cmake.build_config)

        self.output.info("cwd: '%s' cmd: '%s'" % (build_dir, cmd))

        self.run(cmd, cwd=build_dir)

        
    def cmake_defs(self):
        """Generate arguments for cmake"""

        if not hasattr(self, 'package_folder'):
            self.package_folder = "dist"

        args = {
                "OPENDHT_SHARED" : self.options.shared,
                "OPENDHT_STATIC" : not self.options.shared,
                "OPENDHT_TOOLS"  : self.options.build_tools,

                'CMAKE_POSITION_INDEPENDENT_CODE' : self.options.use_pic,
                'CMAKE_INSTALL_PREFIX' : self.package_folder,
                'CMAKE_INSTALL_LIBDIR' : "lib"
        }

        return args
